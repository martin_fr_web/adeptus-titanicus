

function titan(type, leftArm, rightArm, carapace) {
	return {};
};

const WARLORD = {
		type: 'Warlord',
		scale: '10 (magnificus)',
		cost: 385,
		
		command: '3+',
		speed: '4"/6"',
		manoeuvre: '1/2',
		ballisticSkill: '3+',
		weaponSkill: '5+',
		servitorClades: '4',
		
		reactor: ['G',
			['G'],
			['Y'],
			['Y'],
			['O'],
			['O'],
			['R']
		],
		shields: [
			['G', '3+'],
			['Y', '3+'],
			['Y', '3+'],
			['O', '4+'],
			['O', '4+'],
			['R', 'X']
		],
		headLocation: {
			damages: [
				['G', ''],
				['B', ''],
				['B', ''],
				['B', '+1'],
				['B', '+1'],
				['B', '+2'],
				['R', '+3']
			],
			directHit: '13-14',
			devastatingHit: '15-16',
			criticalHit: '17+',
			criticals: [
				['', []],
				['Y', ['MIU Feedback']],
				['O', ['MIU Feedback', 'Moderati Wounded']],
				['R', ['Moderati Wounded', 'Princeps Wounded']]
			]
		},
		bodyLocation: {
			damages: [
				['G', ''],
				['B', ''],
				['B', ''],
				['B', '+1'],
				['B', '+1'],
				['B', '+2'],
				['B', '+2'],
				['R', '+3']
			],
			directHit: '12-13',
			devastatingHit: '14-15',
			criticalHit: '16+',
			criticals: [
				['', []],
				['Y', ['Reactor Leak (1)']],
				['O', ['Reactor Leak (1)', 'VSG Burnout']],
				['R', ['Reactor Leak (2)', 'VSG Burnout']]
			]
		},
		legsLocation: {
			damages: [
				['G', ''],
				['B', ''],
				['B', ''],
				['B', '+1'],
				['B', '+1'],
				['B', '+2'],
				['B', '+2'],
				['R', '+3']
			],
			directHit: '13-14',
			devastatingHit: '15-16',
			criticalHit: '17+',
			criticals: [
				['', []],
				['Y', ['Stabilisers Damaged']],
				['O', ['Stabilisers Damaged', 'Locomotors Seized']],
				['R', ['Immobilised']]
			]
		}
	};
const WARLORD_WEAPON = {
		armor: '11+',
		damages: [
			['11-14', 'Detonation {Body, S7}'],
			['15+', 'Detonation {Body, S9}']
		]
	};

const REAVER = {
		type: 'Reaver',
		scale: '8 (immensus)',
		cost: 250,
		
		command: '4+',
		speed: '6"/9"',
		manoeuvre: '2/3',
		ballisticSkill: '3+',
		weaponSkill: '4+',
		servitorClades: '3',
		
		reactor: ['G',
			['G'],
			['Y'],
			['Y'],
			['O'],
			['R']
		],
		shields: [
			['G', '3+'],
			['Y', '3+'],
			['Y', '4+'],
			['O', '4+'],
			['R', 'X']
		],
		headLocation: {
			damages: [
				['G', ''],
				['B', ''],
				['B', ''],
				['B', '+1'],
				['B', '+2'],
				['R', '+3']
			],
			directHit: '11-13',
			devastatingHit: '14-16',
			criticalHit: '17+',
			criticals: [
				['', []],
				['Y', ['MIU Feedback']],
				['O', ['MIU Feedback', 'Moderati Wounded']],
				['R', ['Moderati Wounded', 'Princeps Wounded']]
			]
		},
		bodyLocation: {
			damages: [
				['G', ''],
				['B', ''],
				['B', ''],
				['B', '+1'],
				['B', '+1'],
				['B', '+2'],
				['R', '+3']
			],
			directHit: '10-12',
			devastatingHit: '13-14',
			criticalHit: '15+',
			criticals: [
				['', []],
				['Y', ['Reactor Leak (1)']],
				['O', ['Reactor Leak (1)', 'VSG Burnout']],
				['R', ['VReactor Leak (2)', 'VSG Burnout']]
			]
		},
		legsLocation: {
			damages: [
				['G', ''],
				['B', ''],
				['B', ''],
				['B', '+1'],
				['B', '+1'],
				['B', '+2'],
				['R', '+3']
			],
			directHit: '11-12',
			devastatingHit: '13-14',
			criticalHit: '15+',
			criticals: [
				['', []],
				['Y', ['Stabilisers Damaged']],
				['O', ['Stabilisers Damaged', 'Locomotors Seized']],
				['R', ['Immobilised']]
			]
		}
	};
const REAVER_WEAPON = {
		armor: '10+',
		damages: [
			['10-13', 'Detonation {Body, S7}'],
			['14+', 'Detonation {Body, S9}']
		]
	};

const WARHOUND = {
		type: 'Warhound',
		scale: '6 (grandis)',
		cost: 180,
		
		command: '5+',
		speed: '8"/12"',
		manoeuvre: '3/5',
		ballisticSkill: '3+',
		weaponSkill: '4+',
		servitorClades: '2',
		
		reactor: ['G',
			['Y'],
			['O'],
			['O'],
			['R']
		],
		shields: [
			['G', '3+'],
			['O', '4+'],
			['O', '4+'],
			['R', 'X']
		],
		headLocation: {
			damages: [
				['G', ''],
				['B', ''],
				['B', '+1'],
				['B', '+2'],
				['R', '+3']
			],
			directHit: '11-13',
			devastatingHit: '14-15',
			criticalHit: '16+',
			criticals: [
				['', []],
				['Y', ['MIU Feedback']],
				['O', ['MIU Feedback', 'Moderati Wounded']],
				['R', ['Moderati Wounded', 'Princeps Wounded']]
			]
		},
		bodyLocation: {
			damages: [
				['G', ''],
				['B', ''],
				['B', '+1'],
				['B', '+1'],
				['B', '+2'],
				['R', '+3']
			],
			directHit: '10-11',
			devastatingHit: '12-13',
			criticalHit: '14+',
			criticals: [
				['', []],
				['Y', ['Reactor Leak (1)']],
				['O', ['Reactor Leak (1)', 'VSG Burnout']],
				['R', ['VReactor Leak (2)', 'VSG Burnout']]
			]
		},
		legsLocation: {
			damages: [
				['G', ''],
				['B', ''],
				['B', '+1'],
				['B', '+1'],
				['B', '+2'],
				['R', '+3']
			],
			directHit: '10-12',
			devastatingHit: '13-14',
			criticalHit: '15+',
			criticals: [
				['', []],
				['Y', ['Stabilisers Damaged']],
				['O', ['Stabilisers Damaged', 'Locomotors Seized']],
				['R', ['Immobilised']]
			]
		}
	};
const WARHOUND_WEAPON = {
		armor: '9+',
		damages: [
			['9-12', 'Detonation {Body, S7}'],
			['13+', 'Detonation {Body, S9}']
		]
	};


const TITAN_CLASSES = {
		warlord: WARLORD,
		reaver: REAVER,
		warhound: WARHOUND
	};



function warlordArmWeapon(weaponStats) {
	return weapon('Warlord Arm', WARLORD_WEAPON, weaponStats);
}
function warlordCarapaceWeapon(weaponStats) {
	return weapon('Warlord Carapace', WARLORD_WEAPON, weaponStats);
}
function reaverArmWeapon(weaponStats) {
	return weapon('Reaver Arm', REAVER_WEAPON, weaponStats);
}
function reaverCarapaceWeapon(weaponStats) {
	return weapon('Reaver Carapace', REAVER_WEAPON, weaponStats);
}
function warhoundArmWeapon(weaponStats) {
	return weapon('Warhound Arm', WARHOUND_WEAPON, weaponStats);
}

function weapon(weaponType, weaponStructure, weaponStats) {
	return Object.assign({}, {type: weaponType}, weaponStats, weaponStructure);
}


const ARIOCH_TITAN_POWER_CLAW = warlordArmWeapon({
		name: 'Arioch Titan Power Claw',
		cost: 25,

		arc: '90',
		shortRange: ['2"', '+2'],
		longRange: ['-', '-'],
		dice: 3,
		strength: 12,
		traits: ['Concussive', 'Melee'],
		
		repair: '2+'
	});
const BELICOSA_VOLCANO_CANNON = warlordArmWeapon({
		name: 'Belicosa Volcano Cannon',
		cost: 55,

		arc: '90',
		shortRange: ['30"', '-'],
		longRange: ['60"', '-'],
		dice: 1,
		strength: 12,
		traits: ['Blast {5"}', 'Draining'],
		
		repair: '5+'
	});
const MACRO_GATLING_BLASTER = warlordArmWeapon({
		name: 'Macro-Gatling Blaster',
		cost: 30,

		arc: '90',
		shortRange: ['8"', '+1'],
		longRange: ['24"', '-'],
		dice: 6,
		strength: 7,
		traits: ['Ordnance'],
		
		repair: '3+'
	});
const MORI_QUAKE_CANNON = warlordArmWeapon({
		name: 'Mori Quake Cannon',
		cost: 20,

		arc: '90',
		shortRange: ['24"', '-1'],
		longRange: ['72"', '-'],
		dice: 1,
		strength: 9,
		traits: ['Blast {5"}', 'Concussive', 'Quake'],
		
		repair: '3+'
	});
const SUNFURY_PLASMA_ANNIHILATOR = warlordArmWeapon({
		name: 'Sunfury Plasma Annihilator',
		cost: 45,

		arc: '90',
		shortRange: ['12"', '-'],
		longRange: ['24"', '-'],
		dice: 4,
		strength: 8,
		traits: ['Maximal Fire'],
		
		repair: '5+'
	});
const APOCALYPSE_MISSILE_LAUNCHERS = warlordCarapaceWeapon({
		name: 'Apocalypse Missile Launchers',
		cost: 15,

		arc: '0',
		shortRange: ['30"', '-'],
		longRange: ['120"', '+1'],
		dice: 10,
		strength: 4,
		traits: ['Barrage', 'Carapace', 'Paired'],
		
		repair: '4+'
	});
const PAIRED_GATLING_BLASTERS = warlordCarapaceWeapon({
		name: 'Paired Gatling Blasters',
		cost: 30,

		arc: '90',
		shortRange: ['8"', '+1'],
		longRange: ['24"', '-'],
		dice: 12,
		strength: 5,
		traits: ['Carapace', 'Ordnance', 'Paired'],
		
		repair: '3+'
	});
const PAIRED_LASER_BLASTERS = warlordCarapaceWeapon({
		name: 'Paired Laser Blasters',
		cost: 50,

		arc: '0',
		shortRange: ['16"', '-'],
		longRange: ['32"', '-1'],
		dice: 6,
		strength: 8,
		traits: ['Carapace', 'Paired', 'Shieldbane {Draining}'],
		
		repair: '4+'
	});
const PAIRED_TURBO_LASER_DESTRUCTORS = warlordCarapaceWeapon({
		name: 'Paired Turbo Laser Destructors',
		cost: 35,

		arc: '0',
		shortRange: ['18"', '-'],
		longRange: ['32"', '-'],
		dice: 4,
		strength: 8,
		traits: ['Carapace', 'Paired', 'Shieldbane {Draining}'],
		
		repair: '4+'
	});
const VULCAN_MEGA_BOLTER_ARRAY = warlordCarapaceWeapon({
		name: 'Vulcan Mega-Bolter Array',
		cost: 20,

		arc: '0',
		shortRange: ['10"', '+1'],
		longRange: ['20"', '-'],
		dice: 12,
		strength: 4,
		traits: ['Carapace', 'Paired', 'Rapid'],
		
		repair: '3+'
	});

const GATLING_BLASTER = reaverArmWeapon({
		name: 'Gatling Blaster',
		cost: 15,

		arc: '90',
		shortRange: ['8"', '+1'],
		longRange: ['24"', '-'],
		dice: 6,
		strength: 5,
		traits: ['Ordnance'],
		
		repair: '3+'
	});
const LASER_BLASTER = reaverArmWeapon({
		name: 'Laser Blaster',
		cost: 25,

		arc: '90',
		shortRange: ['16"', '-'],
		longRange: ['32"', '-1'],
		dice: 3,
		strength: 8,
		traits: ['Shieldbane {Draining}'],
		
		repair: '4+'
	});
const MELTA_CANNON = reaverArmWeapon({
		name: 'Melta Cannon',
		cost: 35,

		arc: '90',
		shortRange: ['12"', '-'],
		longRange: ['24"', '-1'],
		dice: 1,
		strength: 11,
		traits: ['Blast {3"}', 'Fusion'],
		
		repair: '4+'
	});
const REAVER_TITAN_CHAINFIST = reaverArmWeapon({
		name: 'Reaver Titan Chainfist',
		cost: 20,

		arc: '90',
		shortRange: ['2"', '+2'],
		longRange: ['-', '-'],
		dice: 3,
		strength: 8,
		traits: ['Melee', 'Rending'],
		
		repair: '2+'
	});
const REAVER_TITAN_POWER_FIST = reaverArmWeapon({
		name: 'Reaver Titan Power Fist',
		cost: 20,

		arc: '90',
		shortRange: ['2"', '+1'],
		longRange: ['-', '-'],
		dice: 2,
		strength: 9,
		traits: ['Concussive', 'Melee'],
		
		repair: '2+'
	});
const VOLCANO_CANNON = reaverArmWeapon({
		name: 'Volcano Cannon',
		cost: 25,

		arc: '90',
		shortRange: ['30"', '-'],
		longRange: ['60"', '-'],
		dice: 1,
		strength: 10,
		traits: ['Blast {3"}', 'Draining'],
		
		repair: '5+'
	});
const APOCALYPSE_MISSILE_LAUNCHER = reaverCarapaceWeapon({
		name: 'Apocalypse Missile Launcher',
		cost: 10,

		arc: '360',
		shortRange: ['30"', '-'],
		longRange: ['120"', '+1'],
		dice: 5,
		strength: 4,
		traits: ['Barrage', 'Carapace'],
		
		repair: '4+'
	});
const REAVER_TITAN_TURBO_LASER_DESTRUCTOR = reaverCarapaceWeapon({
		name: 'Turbo Laser Destructor',
		cost: 20,

		arc: '360',
		shortRange: ['18"', '-'],
		longRange: ['32"', '-'],
		dice: 2,
		strength: 8,
		traits: ['Carapace', 'Shieldbane {Draining}'],
		
		repair: '4+'
	});
const REAVER_TITAN_VULCAN_MEGA_BOLTER = reaverCarapaceWeapon({
		name: 'Vulcan Mega-Bolter',
		cost: 10,

		arc: '360',
		shortRange: ['8"', '-'],
		longRange: ['20"', '-'],
		dice: 6,
		strength: 4,
		traits: ['Carapace', 'Rapid'],
		
		repair: '3+'
	});
const WARP_MISSILE_SUPPORT_RACK = reaverCarapaceWeapon({
		name: 'Warp Missile Support Rack',
		cost: 10,

		arc: '360',
		shortRange: ['20"', '+1'],
		longRange: ['80"', '+2'],
		dice: 1,
		strength: 'X',
		traits: ['Carapace', 'Limied {1}', 'Warp'],
		
		repair: '3+'
	});

const INFERNO_GUN = warhoundArmWeapon({
		name: 'Inferno Gun',
		cost: 20,

		arc: '90',
		shortRange: ['T', '-'],
		longRange: ['-', '-'],
		dice: 3,
		strength: 7,
		traits: ['Firestorm'],
		
		repair: '2+'
	});
const PLASMA_BLASTGUN = warhoundArmWeapon({
		name: 'Plasma Blastgun',
		cost: 30,

		arc: '90',
		shortRange: ['8"', '-'],
		longRange: ['24"', '-1'],
		dice: 2,
		strength: 8,
		traits: ['Blast {3"}', 'Maximal Fire'],
		
		repair: '5+'
	});
const TURBO_LASER_DESTRUCTOR = warhoundArmWeapon({
		name: 'Turbo Laser Destructor',
		cost: 20,

		arc: '90',
		shortRange: ['16"', '-'],
		longRange: ['32"', '-'],
		dice: 2,
		strength: 8,
		traits: ['Shieldbane {Draining}'],
		
		repair: '4+'
	});
const VULCAN_MEGA_BOLTER = warhoundArmWeapon({
		name: 'Vulcan Mega-Bolter',
		cost: 10,

		arc: '90',
		shortRange: ['8"', '+1'],
		longRange: ['20"', '-'],
		dice: 6,
		strength: 4,
		traits: ['Rapid'],

		repair: '3+'
	});

const WEAPONS = [
		ARIOCH_TITAN_POWER_CLAW,
		BELICOSA_VOLCANO_CANNON,
		MACRO_GATLING_BLASTER,
		MORI_QUAKE_CANNON,
		SUNFURY_PLASMA_ANNIHILATOR,
		APOCALYPSE_MISSILE_LAUNCHERS,
		PAIRED_GATLING_BLASTERS,
		PAIRED_LASER_BLASTERS,
		PAIRED_TURBO_LASER_DESTRUCTORS,
		VULCAN_MEGA_BOLTER_ARRAY,
		GATLING_BLASTER,
		LASER_BLASTER,
		MELTA_CANNON,
		REAVER_TITAN_CHAINFIST,
		REAVER_TITAN_POWER_FIST,
		VOLCANO_CANNON,
		APOCALYPSE_MISSILE_LAUNCHER,
		REAVER_TITAN_TURBO_LASER_DESTRUCTOR,
		REAVER_TITAN_VULCAN_MEGA_BOLTER,
		WARP_MISSILE_SUPPORT_RACK,
		INFERNO_GUN,
		PLASMA_BLASTGUN,
		TURBO_LASER_DESTRUCTOR,
		VULCAN_MEGA_BOLTER
	];
	
	
const COLOR = {
	G : 'green',
	B : 'blue',
	Y : 'yellow',
	O : 'orange',
	R : 'red'
};


new Vue({
	el: '#app',
	
	data: {
		setup: {
			mode: true,
			titan: '',
			leftArm: '',
			rightArm: '',
			carapace: ''
		},
		
		
		titan: {},
		leftArm: {},
		rightArm: {},
		carapace: {},

		name: 'Silver Regent',
		current: {
			reactor: 0,
			shield: 0,

			headLocation: 0,
			headCritical: 0,

			bodyLocation: 0,
			bodyCritical: 0,

			legsLocation: 0,
			legsCritical: 0,

			leftArm: true,
			rightArm: true,
			carapace: true,
		}
	},
	
	computed: {
		armWeaponsAvailable: () => (titan) => {
				const weaponType = titan.type + ' Arm';
				return WEAPONS.filter(weapon => weapon.type === weaponType);
			},
		carapaceWeaponsAvailable: () => (titan) => {
				const weaponType = titan.type + ' Carapace';
				return WEAPONS.filter(weapon => weapon.type === weaponType);
			},
		
		titanTypes: function (){return TITAN_CLASSES;},
		
		color: () => (value) => {return COLOR[value];},
		
		cost: function (){
				let cost = 0;
				if (this.setup.mode) {
					cost += this.setup.titan ? this.setup.titan.cost : 0;
					cost += this.setup.leftArm ? this.setup.leftArm.cost : 0;
					cost += this.setup.rightArm ? this.setup.rightArm.cost : 0;
					cost += this.setup.carapace ? this.setup.carapace.cost : 0;
				} else {
					cost += this.titan ? this.titan.cost : 0;
					cost += this.leftArm ? this.leftArm.cost : 0;
					cost += this.rightArm ? this.rightArm.cost : 0;
					cost += this.carapace ? this.carapace.cost : 0;
				}
				return cost;
			},

		reactorMax: function (){return this.titan.reactor.length - 1;},
		reactorLevel: function (){return this.titan.reactor[this.current.reactor];},

		shieldMax: function (){return this.titan.shields.length - 1;},
		shieldLevel: function (){return this.titan.shields[this.current.shield];},

		headLocationMax: function (){return this.titan.headLocation.damages.length - 1;},
		headLocationLevel: function (){return this.titan.headLocation.damages[this.current.headLocation];},
		headCriticalMax: function (){return this.titan.headLocation.criticals.length - 1;},
		headCriticalLevel: function (){return this.titan.headLocation.criticals[this.current.headCritical];},

		bodyLocationMax: function (){return this.titan.bodyLocation.damages.length - 1;},
		bodyLocationLevel: function (){return this.titan.bodyLocation.damages[this.current.bodyLocation];},
		bodyCriticalMax: function (){return this.titan.bodyLocation.criticals.length - 1;},
		bodyCriticalLevel: function (){return this.titan.bodyLocation.criticals[this.current.bodyCritical];},

		legsLocationMax: function (){return this.titan.legsLocation.damages.length - 1;},
		legsLocationLevel: function (){return this.titan.legsLocation.damages[this.current.legsLocation];},
		legsCriticalMax: function (){return this.titan.legsLocation.criticals.length - 1;},
		legsCriticalLevel: function (){return this.titan.legsLocation.criticals[this.current.legsCritical];}
	},

	methods: {
		// setup
		startGame() {
			this.titan = this.setup.titan;
			this.leftArm = this.setup.leftArm;
			this.rightArm = this.setup.rightArm;
			this.carapace = this.setup.carapace;
			this.setup.mode = false;
		},
		
		
		// reactor
		pushReactor() {
			const currentReactor = this.current.reactor;
			if (currentReactor < this.reactorMax) {
				this.current.reactor = currentReactor + 1;
			}
		},
		ventReactor() {
			const currentReactor = this.current.reactor;
			if (currentReactor > 0) {
				this.current.reactor = currentReactor - 1;
			}
		},

		// weapons
		disableLeftArmWeapon() {
			this.current.leftArm = false;
		},
		repairLeftArmWeapon() {
			this.current.leftArm = true;
		},
		disableRightArmWeapon() {
			this.current.rightArm = false;
		},
		repairRightArmWeapon() {
			this.current.rightArm = true;
		},
		disableCarapaceWeapon() {
			this.current.carapace = false;
		},
		repairCarapaceWeapon() {
			this.current.carapace = true;
		},
		
		// shield
		loseShield() {
			const currentShield = this.current.shield;
			if (currentShield < this.shieldMax) {
				this.current.shield = currentShield + 1;
			}
		},
		riseShield() {
			const currentShield = this.current.shield;
			if (currentShield > 0) {
				this.current.shield = currentShield - 1;
			}
		},
		
		// head
		directHitToHead() {
			const currentHeadLocation = this.current.headLocation;
			if (currentHeadLocation < this.headLocationMax) {
				this.current.headLocation = currentHeadLocation + 1;
			} else {
				this.criticalDamageToHead();
			}
		},
		devastatingHitToHead() {
			const currentHeadLocation = this.current.headLocation;
			if ((currentHeadLocation + 2) <= this.headLocationMax) {
				this.current.headLocation = currentHeadLocation + 2;
			} else {
				this.current.headLocation = this.headLocationMax;
				this.criticalDamageToHead();
			}
		},
		criticalHitToHead() {
			this.criticalDamageToHead();
			this.devastatingHitToHead();
		},
		criticalDamageToHead() {
			const currentHeadCritical = this.current.headCritical;
			if (currentHeadCritical < this.headCriticalMax) {
				this.current.headCritical = currentHeadCritical + 1;
			}
		},
		repairCriticalToHead() {
			const currentHeadCritical = this.current.headCritical;
			if (currentHeadCritical > 0) {
				this.current.headCritical = currentHeadCritical - 1;
			}
		},
		cancelDamageToHeadLocation() {
			const currentHeadLocation = this.current.headLocation;
			if (currentHeadLocation > 0) {
				this.current.headLocation = currentHeadLocation - 1;
			}
		},

		// body
		directHitToBody() {
			const currentBodyLocation = this.current.bodyLocation;
			if (currentBodyLocation < this.bodyLocationMax) {
				this.current.bodyLocation = currentBodyLocation + 1;
			} else {
				this.criticalDamageToBody();
			}
		},
		devastatingHitToBody() {
			const currentBodyLocation = this.current.bodyLocation;
			if ((currentBodyLocation + 2) <= this.bodyLocationMax) {
				this.current.bodyLocation = currentBodyLocation + 2;
			} else {
				this.current.bodyLocation = this.bodyLocationMax;
				this.criticalDamageToBody();
			}
		},
		criticalHitToBody() {
			this.criticalDamageToBody();
			this.devastatingHitToBody();
		},
		criticalDamageToBody() {
			const currentBodyCritical = this.current.bodyCritical;
			if (currentBodyCritical < this.bodyCriticalMax) {
				this.current.bodyCritical = currentBodyCritical + 1;
			}
		},
		repairCriticalToBody() {
			const currentBodyCritical = this.current.bodyCritical;
			if (currentBodyCritical > 0) {
				this.current.bodyCritical = currentBodyCritical - 1;
			}
		},
		cancelDamageToBodyLocation() {
			const currentBodyLocation = this.current.bodyLocation;
			if (currentBodyLocation > 0) {
				this.current.bodyLocation = currentBodyLocation - 1;
			}
		},
		
		// legs
		directHitToLegs() {
			const currentLegsLocation = this.current.legsLocation;
			if (currentLegsLocation < this.legsLocationMax) {
				this.current.legsLocation = currentLegsLocation + 1;
			} else {
				this.criticalDamageToLegs();
			}
		},
		devastatingHitToLegs() {
			const currentLegsLocation = this.current.legsLocation;
			if ((currentLegsLocation + 2) <= this.legsLocationMax) {
				this.current.legsLocation = currentLegsLocation + 2;
			} else {
				this.current.legsLocation = this.legsLocationMax;
				this.criticalDamageToLegs();
			}
		},
		criticalHitToLegs() {
			this.criticalDamageToLegs();
			this.devastatingHitToLegs();
		},
		criticalDamageToLegs() {
			const currentLegsCritical = this.current.legsCritical;
			if (currentLegsCritical < this.legsCriticalMax) {
				this.current.legsCritical = currentLegsCritical + 1;
			}
		},
		repairCriticalToLegs() {
			const currentLegsCritical = this.current.legsCritical;
			if (currentLegsCritical > 0) {
				this.current.legsCritical = currentLegsCritical - 1;
			}
		},
		cancelDamageToLegsLocation() {
			const currentLegsLocation = this.current.legsLocation;
			if (currentLegsLocation > 0) {
				this.current.legsLocation = currentLegsLocation - 1;
			}
		}
	},

	created() {
//		this.titan = WARLORD;
//		this.leftArm = sunfury_plasma_annihilator();
//		this.rightArm = mori_quake_cannon();
//		this.carapace = vulcan_mega_bolter_array();
		
//		this.titan = REAVER;
//		this.leftArm = VULCAN_MEGA_BOLTER;
//		this.rightArm = volcano_cannon();
//		this.carapace = warp_missile_support_rack();

//		this.titan = WARHOUND;
//		this.leftArm = TURBO_LASER_DESTRUCTOR;
//		this.rightArm = VULCAN_MEGA_BOLTER;
//		this.carapace = null;
	}
});